load("@com_github_ash2k_bazel_tools//multirun:def.bzl", "multirun")
load("@rules_proto_grpc//:defs.bzl", "proto_plugin")

exports_files([
    "buildozer_commands.txt",
    "validate_dependency.patch",
])

# See https://github.com/rules-proto-grpc/rules_proto_grpc#developing-custom-plugins
# See https://github.com/rules-proto-grpc/rules_proto_grpc/issues/16
proto_plugin(
    name = "go_validate_plugin",
    exclusions = [
        "google/protobuf",
        "validate",
    ],
    options = [
        "lang=go",
        "paths=source_relative",
    ],
    outputs = ["{protopath}.pb.validate.go"],
    tool = "@com_github_envoyproxy_protoc_gen_validate//:protoc-gen-validate",
    visibility = ["//visibility:public"],
)

sh_binary(
    name = "copy_absolute",
    srcs = ["copy_absolute.sh"],
    visibility = ["//visibility:public"],
)

sh_binary(
    name = "copy_to_workspace",
    srcs = ["copy_to_workspace.sh"],
    visibility = ["//visibility:public"],
)

multirun(
    name = "extract_generated_proto",
    commands = [
        "//cmd/kas/kasapp:extract_generated",
        "//internal/gitlab/api:extract_generated",
        "//internal/module/agent_configuration/rpc:extract_generated",
        "//internal/module/agent_tracker/rpc:extract_generated",
        "//internal/module/agent_tracker:extract_generated",
        "//internal/module/configuration_project/rpc:extract_generated",
        "//internal/module/gitlab_access/rpc:extract_generated",
        "//internal/module/gitops/rpc:extract_generated",
        "//internal/module/kubernetes_api/rpc:extract_generated",
        "//internal/module/modserver:extract_generated",
        "//internal/module/modshared:extract_generated",
        "//internal/module/reverse_tunnel/info:extract_generated",
        "//internal/module/reverse_tunnel/rpc:extract_generated",
        "//internal/module/reverse_tunnel/tracker:extract_generated",
        "//internal/tool/grpctool/automata:extract_generated",
        "//internal/tool/grpctool/test:extract_generated",
        "//internal/tool/grpctool:extract_generated",
        "//internal/tool/prototool:extract_generated",
        "//internal/tool/redistool:extract_generated",
        "//pkg/agentcfg:extract_generated",
        "//pkg/kascfg:extract_generated",
        "//pkg/ruby:extract_modserver_pb",
        "//pkg/ruby:extract_modshared_pb",
        "//pkg/ruby:extract_agent_tracker_pb",
        "//pkg/ruby:extract_agent_tracker_rpc_pb",
        "//pkg/ruby:extract_agent_tracker_rpc_services_pb",
        "//pkg/ruby:extract_configuration_project_rpc_pb",
        "//pkg/ruby:extract_configuration_project_rpc_services_pb",
    ],
)

multirun(
    name = "extract_binaries_for_gdk",
    commands = [
        "//cmd/kas:extract_kas",
    ],
)

multirun(
    name = "extract_race_binaries_for_gdk",
    commands = [
        "//cmd/kas:extract_kas_race",
    ],
)
